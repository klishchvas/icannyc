<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/courses/{category?}', [\App\Http\Controllers\CourseController::class, 'index'])
    ->name('courses.index');
Route::get('/courses/{course}/show', [\App\Http\Controllers\CourseController::class, 'show'])
    ->name('courses.show');
Route::get('courses/search/index',[\App\Http\Controllers\CourseController::class, 'search'])
    ->name('courses.search');
