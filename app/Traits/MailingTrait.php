<?php


namespace App\Traits;


use App\Models\Mail;

trait MailingTrait
{
    public $name,$agreement,$email;
    protected $rules =[
        'name' => 'required|min:2',
        'email' => 'required|email|unique:App\Models\Mail,email',
        'agreement' => 'required'
    ];

    public function submit () {
        $this->addError('fail_mailing', config('messages.messages.fail_mailing'));
        $this->validate();

        Mail::create([
            'name' => $this->name,
            'email' => $this->email,
            'agreement' => $this->agreement,
        ]);

        $this->reset(['name','email','agreement']);

        session()->flash('success_message',config('messages.messages.success_mailing'));

    }
}
