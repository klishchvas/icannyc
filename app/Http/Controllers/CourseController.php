<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Course;
use App\Models\Tag;

class CourseController extends Controller
{
    public function index(Category $category)
    {
        $courses = Course::CoursesCategory($category);

        return view('pages.courses',['courses' => $courses]);
    }

    public function show(Course $course)
    {
        return view('pages.course',compact('course'));
    }

    public function search()
    {
        $search = request('courseSearch');
        $courses = Course::where('title','LIKE',"%{$search}%")->get();
        $tags = Tag::where('title','LIKE',"%{$search}%")->get();
        foreach ($tags as $tag){
            $tag->courses->map(function($item) use ($courses){
                $courses[] = $item;
            });
        }

        return view('pages.search',compact('search','courses'));
    }
}
