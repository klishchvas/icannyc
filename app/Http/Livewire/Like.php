<?php

namespace App\Http\Livewire;

use App\Models\Course;
use Livewire\Component;

class Like extends Component
{
    public $course;

    public function mount(Course $course){
        $this->course = $course;
    }

    public function addLike()
    {

        if (!session()->has('is_like')) {

            session(['is_like' => true]);

            $this->course->likes = ($this->course->likes + 1);

            $this->course->save();

        } else if(session()->has('is_like') && $this->course->likes > 0 ) {

            session()->forget('is_like');

            $this->course->likes = ($this->course->likes - 1);

            $this->course->save();

        }else{
            session(['is_like' => true]);

            $this->course->likes = ($this->course->likes + 1);

            $this->course->save();
        }
    }

    public function render()
    {
        return view('livewire.like');
    }
}
