<?php

namespace App\Http\Livewire\Forms\Mailing;

use App\Models\Mail;
use App\Traits\MailingTrait;
use Livewire\Component;

class FormMailing extends Component
{
   use MailingTrait;


    public function render()
    {
        return view('livewire.forms.mailing.form-mailing');
    }
}
