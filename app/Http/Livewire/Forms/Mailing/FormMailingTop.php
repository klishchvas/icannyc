<?php

namespace App\Http\Livewire\Forms\Mailing;

use App\Traits\MailingTrait;
use Livewire\Component;

class FormMailingTop extends Component
{
    use MailingTrait;
    
    public function render()
    {
        return view('livewire.forms.mailing.form-mailing-top');
    }
}
