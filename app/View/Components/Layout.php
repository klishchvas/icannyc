<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Layout extends Component
{
    public $title = 'ICANNYC';
    public $headerClass;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $headerClass = 'header header--home')
    {
        $this->title = $title;
        $this->headerClass = $headerClass;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.layout');
    }
}
