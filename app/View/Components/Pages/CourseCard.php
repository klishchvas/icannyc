<?php

namespace App\View\Components\Pages;

use App\Models\Course;
use App\Models\Theme;
use Illuminate\View\Component;

class CourseCard extends Component
{
    public $course;
    public $theme;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Course $course,Theme $theme)
    {
        //
       $this->course = $course;
       $this->theme = $theme;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if($this->course->is_sale === 0){

            return view('components.pages.course-card-coming');
        }
        return view('components.pages.course-card');
    }
}
