<?php

namespace App\View\Components\Sections;

use App\Models\Review;
use Illuminate\View\Component;

class Reviews extends Component
{
    public $reviews;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->reviews = Review::all();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.sections.reviews');
    }
}
