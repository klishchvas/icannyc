<?php

namespace App\View\Components\Sections;

use App\Models\Answer;
use Illuminate\View\Component;

class Answers extends Component
{
    public $answers = null;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->answers = Answer::all();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.sections.answers');
    }
}
