<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Theme extends Model
{
    use HasFactory;

    public function getImgAttribute()
    {
        return asset(Storage::url($this->attributes['img']));
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }
}
