<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Course extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'raiting', 'picture', 'picture_mobile', 'price', 'old_price', 'theme'];

    protected $casts = [
//        2021-10-15T18:43:00
        'date_timer' => 'datetime:Y-m-dTH:i:s'
    ];

    public static function getTimerFormat()
    {
        return Carbon::now()->addHours(10)->format('Y-m-d\TH:i:s');
    }

    public function getRouteKeyName()
    {
        return 'title';
    }

    public function getPictureAttribute()
    {
        return asset(Storage::url($this->attributes['picture']));
    }

    public function getPictureMobileAttribute()
    {
        if (!is_null($this->attributes['picture_mobile']))
            return asset(Storage::url($this->attributes['picture_mobile']));
        return null;
    }

    public function getLikesAttribute(){
        return !empty($this->attributes['likes']) ? $this->attributes['likes'] : 0;
}

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function theme(){
        return $this->hasOne(Theme::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function author(){

        return $this->hasOne(Author::class);

    }

    public function scopeCoursesCategory($query,$category)
    {

        if($category->id === null){
            return $query->get();
        }

        return $category->courses;

    }


}
