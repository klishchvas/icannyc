<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    use HasFactory;

    protected $fillable = ['name','email','agreement'];
    protected $hidden = ['created_at', 'updated_at'];

    public function getAgreementAttribute(){
        if($this->attributes['agreement'])
            return true;
        return false;
    }
}
