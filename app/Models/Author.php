<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Author extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'img','about', 'position'];

    public function getImgAttribute() {

        return  asset(Storage::url($this->attributes['img']));

    }

    public function course(){

        return $this->belongsTo(Course::class);

    }
}
