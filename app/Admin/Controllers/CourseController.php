<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Models\Course;
use App\Models\Tag;
use App\Models\Theme;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;

class CourseController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Курсы';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Course());

        $grid->column('id', __('Id'))->hide();

        $grid->column('title', __('Название курса'));

        $grid->column('author.name',__('Автор курса'));

        $grid->column('raiting', __('Рейтинг '));

        $grid->column('likes', __('Количество лайков'))->width(100)->limit(30)->display(function ($count) {
            return $count > 0 ? $count : 0;
        });

        $grid->column('video', __('Видео курса'))->width(100)->limit(30);

        $grid->picture('Обложка')->display(function ($picture) {
            return "<img width='40' src='$picture' alt=''/>";
        });

        $grid->picture_mobile('Обложка для мобилок')->display(function ($picture) {
            return "<img width='40' src='$picture' alt=''/>";
        });

        $grid->categories('категории')->width(200)->display(function ($categories) {

            $categories = array_map(function ($category) {

                return '<a tabindex="0" class="btn btn-xs btn-twitter" role="button" data-toggle="popover" data-html="true"

               title="Usage">' . $category['title'] . '</a>';

            }, $categories);

            return join('&nbsp;', $categories);
        });

        $grid->column('description', __('Описание'))->width(100)->limit(20);

        $grid->column('price', __('Цена'));

        $grid->column('old_price', __('Старая цена'));

        $grid->is_sale('В продаже')->display(function ($is_sale) {

            return $is_sale ? "Да" : "Нет";
        });

        $grid->column('created_at', __('Created at'))->hide();

        $grid->column('updated_at', __('Updated at'))->hide();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Course::findOrFail($id));

        $show->field('title', __('Название курса'));

        /**
         * Поле с Автором курса
         * ====================
         */

        $show->author('Об авторе', function ($author) {

            $author->setResource('/admin/authors');

            $author->id();
            $author->name();
            $author->email();

            /* Функционал */
            $author->actions(function ($actions) {
                $actions->disableDelete();
                $actions->disableEdit();
                $actions->disableView();
            });
            $author->disableCreateButton();
            $author->disableFilter();
            $author->disableExport();
            $author->disableExport();
        });

        /* ========================= */


        $show->field('raiting', __('Рейтинг курса'));

        $show->field('likes', __('Количество лайков'))->as(function ($count){
            return $count > 0 ? $count : 0;
        });

        $show->picture('обложка')->image();

        $show->picture_mobile('обложка для мобилок')->image();

        $show->is_sale("В продаже")->as(function ($is_sale) {
            return $is_sale ? "Да" : "Нет";

        });

        $show->field('description', __('Описание'));

        $show->field('video', __('Видео курса'));


        $show->field('created_at', __('Создано'));

        $show->field('updated_at', __('Обновлено'));

        /**
         * Поле с тегами курса
         * ====================
         */
        $show->tags('Теги', function ($tags) {
            $tags->resource('/admin/tags');

            $tags->title('Тэг');

            /* Функционал */
            $tags->actions(function ($actions) {
                $actions->disableDelete();
                $actions->disableEdit();
                $actions->disableView();
            });
            $tags->disableCreateButton();
            $tags->disableFilter();
            $tags->disableExport();
            $tags->disableExport();
        });
        /* ========================= */

        /**
         * Поле с категориями курса
         * ====================
         */

        $show->categories('Категории', function ($categories) {

            $categories->resource('/admin/categories');

            $categories->id()->hide();
            $categories->title('Название категории')->limit(10);
            $categories->created_at()->hide();
            $categories->updated_at()->hide();

            /* Функционал */
            $categories->actions(function ($actions) {
                $actions->disableDelete();
                $actions->disableEdit();
                $actions->disableView();
            });
            $categories->disableCreateButton();
            $categories->disableFilter();
            $categories->disableExport();
            $categories->disableExport();


        });
        /* ========================= */

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Course());

        $form->text('title', __('Название курса'))->placeholder('Введите название курса')->required();

        $form->decimal('raiting', __('Рейтинг'))->placeholder('Укажите рейтинг');

        $form->image('picture', __('Обложка'))->required();

        $form->image('picture_mobile', __('Обложка для мобилы'));

        $form->decimal('price', __('Цена'))->placeholder('Укажите цену')->required();

        $form->decimal('old_price', __('Старая цена'))->placeholder('Укажите старую цену');

        $form->text('video', __('Видео'));


        $form->switch('is_sale', __('В продаже ?'));

        $form->datetime('date_timer','Конец акции');

        $form->textarea('description', __('Описание курса'))->required()

            ->placeholder('Введите описание курса');

        $form->multipleSelect('tags', 'Тэг')
            ->options(Tag::all()->pluck('title', 'id'));


        $form->multipleSelect('categories', 'Категории')

            ->options(Category::all()->pluck('title', 'id'))->required();



        return $form;
    }
}
