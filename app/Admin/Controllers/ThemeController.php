<?php

namespace App\Admin\Controllers;

use App\Models\Course;
use App\Models\Theme;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ThemeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Темы';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Theme());

        $grid->column('id', __('Id'))->hide();
        $grid->column('name', __('Название темы'));
        $grid->column('img', __('Логотип темы'));
        $grid->column('is_show',__('Активная тема?'))->display(function ($is_show){
            return $is_show ? "Да" : "Нет";
        });

        $grid->column('slug', __('slug'));
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();
        $grid->course()->title('Курс');


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Theme::findOrFail($id));

        $show->field('name', __('Название темы'));
        $show->field('img', __('Логотип темы'));
        $show->field('is_show', __('активная тема?'));
        $show->field('created_at', __('Созданная запись'));
        $show->field('updated_at', __('Обновленнная запись'));
        $show->field('slug', __('Slug'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Theme());

        $form->text('name', __('Название темы'))->required()->placeholder('Введите название темы')->required();
        $form->text('slug', __('Slug'))->required()->placeholder('Введите Slug')->required();
        $form->image('img', __('Логотип темы'))->placeholder('выберите логотип')->required();
        $form->switch('is_show', __('Активная тема'));
        $form->select('course_id',__('Курс'))->options(Course::all()->pluck('title','id'));

        return $form;
    }
}
