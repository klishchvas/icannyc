<?php

namespace App\Admin\Controllers;

use App\Models\Author;
use App\Models\Course;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AuthorController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Авторы';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Author());

        $grid->column('id', __('Id'));

        $grid->column('name', __('Имя автора'));

        $grid->column('img', __('Аватар'))->display(function ($img){

            return "<img width='50' src='$img' alt='' />";

        });

        $grid->column('course.title', __('Курс'));

        $grid->column('created_at', __('Created at'))->hide();

        $grid->column('updated_at', __('Updated at'))->hide();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Author::findOrFail($id));

        $show->field('id', __('Id'));

        $show->field('name', __('Имя автора'));

        $show->img('Аватар')->image();

        $show->field('position', __('Занимаемая должность'));

        $show->field('about', __('Об авторе'));

        $show->field('course_id', __('Course id'));

        $show->field('created_at', __('Created at'));

        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Author());

        $form->text('name', __('Имя автора'))->required()->placeholder('Введите имя автора');

        $form->image('img',__('Аватар'))->required();

        $form->text('position', __('Должность'))->required()->placeholder('введите занимаемую должность');

        $form->textarea('about', __('Об авторе'))->required()->placeholder('Раскажите о себе');

        $form->select('course_id',__('Курс'))->options(Course::all()->pluck('title','id'))

            ->required();


        return $form;
    }
}
