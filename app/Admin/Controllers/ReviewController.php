<?php

namespace App\Admin\Controllers;

use App\Models\Review;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ReviewController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Отзывы';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Review());

        $grid->column('id', __('Id'))->hide();
        $grid->column('name', __('Имя пользователя'));
        $grid->column('img', __('Аватар'))->display(function($img){
            return '<img width="50" src="'.$img.'">';
        });
        $grid->column('city', __('Город'));
        $grid->column('age', __('Возраст'));
        $grid->column('mark', __('Оценка'));
        $grid->column('course', __('Курс'));
        $grid->column('review', __('Отзыв'))->hide();
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Review::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Имя'));
        $show->img('Аватар')->image();
        $show->field('city', __('Город'));
        $show->field('age', __('Возраст'));
        $show->field('mark', __('Оценка'));
        $show->field('course', __('Курс'));
        $show->field('review', __('Отзыв'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Review());

        $form->text('name', __('Имя'))->required()->placeholder('введите имя');
        $form->image('img', __('Аватар'));
        $form->text('city', __('Город'))->required()->placeholder('Введите город');
        $form->text('age', __('Возраст'))->required()->placeholder('возраст');
        $form->number('mark', __('Оценка'))->default(0)->min(0)
            ->max(5)->placeholder('Оценка');
        $form->text('course', __('Курс'))->required()->placeholder('Введите курс');
        $form->textarea('review', __('Отзыв'))->required()->placeholder('Оставьте свой отзыв');

        return $form;
    }
}
