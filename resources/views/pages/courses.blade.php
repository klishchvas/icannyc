<x-layout header-class="header" title="ICANNYC - все курсы">
    <div class="page-bgtop">
        <div class="catalog">
            <div class="container catalog__container">
                <div class="catalog__content">
                    <div class="block-title block-title--md catalog__title">
                        Все программы
                    </div>
                    <div class="tabs catalog__tabs">
                        <div class="tabs__head catalog__tabs-head">
                            <div class="tabs__nav tabs__nav--catalog">
                                @foreach (\App\Models\Category::all() as $category)
                                    <a href="{{ route('courses.index',$category->id) }}" class="btn tabs__nav-btn">{{$category->title}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="tabs__content catalog__list">
                            @foreach ($courses as $course)
                                <x-pages.course-card :course="$course"/>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>
