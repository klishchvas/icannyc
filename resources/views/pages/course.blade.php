<x-layout header-class='header' :title="'ICANNYC | '.$course->title">
    <div class="page-bgtop">
        <div class="course-intro">
            <div class="container course-intro__container">
                <div class="course-intro__inner">
                    <x-pages.course-card :course="$course" />
                    <div class="course-intro__stat">
                        <div class="course-intro__places">
                            <div class="course-intro__places-block">
                                <span class="course-intro__places-text">Всего мест:</span>
                                <span class="course-intro__places-val">105</span>
                            </div>
                            <div class="course-intro__places-block">
                                <span class="course-intro__places-sep">/</span>
                                <span class="course-intro__places-text">Осталось мест:</span>
                                <span class="course-intro__places-val">8</span>
                            </div>
                        </div>
                        <div class="course-intro__purchased">
                            <span class="courses-intro__purchased-val color-red">1937</span>
                            <span class="courses-intro__purchased-text">человек купило!</span>
                        </div>
                    </div>
                    <div class="block-title block-title--md course-intro__title">Что ты получишь купив курс?</div>
                    <div class="course-intro__advantages">
                        <div class="course-intro__advantages-list">
                            <div class="course-intro__advantage-item">Шанс выграть $5000</div>
                            <div class="course-intro__advantage-item">Возможность работать в USA</div>
                            <div class="course-intro__advantage-item">Постоянную поддержку</div>
                        </div>
                        <div class="course-intro__advantages-list">
                            <div class="course-intro__advantage-item">Финансовую независимость</div>
                            <div class="course-intro__advantage-item">Знания в новой сфере</div>
                            <div class="course-intro__advantage-item">Качественное обучение</div>
                        </div>
                        <div class="course-intro__advantages-list">
                            <div class="course-intro__advantage-item">Повышение заработка</div>
                            <div class="course-intro__advantage-item">Возможности</div>
                            <div class="course-intro__advantage-item">Крутые знакомства</div>
                        </div>
                    </div>
                    <button class="btn course-intro__btn-detail" aria-expanded="false">
                        <span>Подробнее</span>
                        <svg class="">
                            <use xlink:href="img/sprite.svg#arrow-bottom"></use>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="course-intro__collapse">
                <div class="course-intro__description">
                    <div class="container course-intro__container">
                        <div class="block-title block-title--sm block-title--black">Описание курса</div>
                        <div class="course-intro__description-text">{{$course->description}}</div>
                    </div>
                </div>
                @if ($course->author)
                    <div class="speaker">
                        <div class="container course-intro__container">
                            <div class="speaker__inner">
                                <div class="speaker__present">
                                    <img class="speaker__present-photo" src="{{$course->author->img}}" alt="">
                                    <div class="speaker__present-info">
                                        <span class="speaker__post color-yellow">{{$course->author->position}}</span>
                                        <span class="speaker__name">{{$course->author->name}}</span>
                                    </div>
                                </div>
                                <div class="speaker__info">
                                    <div class="speaker__info-head">
                                        <span class="speaker__name speaker__name--mob-none">{{$course->author->name}}</span>
                                        <div class="socials speaker__socials">
                                            <a class="socials__link socials__link--hover-yellow" href="">
                                                <svg>
                                                    <use xlink:href="img/sprite.svg#instagram"></use>
                                                </svg>
                                            </a>
                                            <a class="socials__link socials__link--hover-yellow" href="">
                                                <svg>
                                                    <use xlink:href="img/sprite.svg#telegram"></use>
                                                </svg>
                                            </a>
                                            <a class="socials__link socials__link--hover-yellow" href="">
                                                <svg>
                                                    <use xlink:href="img/sprite.svg#whatsapp"></use>
                                                </svg>
                                            </a>
                                            <a class="socials__link socials__link--hover-yellow" href="">
                                                <svg>
                                                    <use xlink:href="img/sprite.svg#vk"></use>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                    <span class="speaker__post speaker__post--red speaker__post--mob-none">{{$course->author->position}}</span>
                                    <div class="speaker__info-text">{{$course->author->about}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        @if ($course->video)
            <div class="video video--full">
                <iframe width="100%" height="100%" src="{{$course->video}}"
                        title="{{$course->title}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write;
                        encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        @endif



        <div class="stages">
            <div class="container stages__container">
                <h2 class="block-title block-title--md stages__title">Как проходят курсы <span
                        class="title-accent">ICANNYC ?</span></h2>
                <div class="stages__inner">
                    <div class="stages__left">
                        <div class="stages__list">
                            <div class="stages__item">
                                <div class="stages__item-count">
                                    <div class="stages__item-count-val">1</div>
                                </div>
                                <div class="stages__item-info">
                                    <div class="stages__item-title">Выбор курса</div>
                                    <div class="stages__item-text">
                                        Первым шагом, для начала изучения нового материала стоит выбор. Необходимо
                                        выбрать интересующую Вас сферу или конкретный курс. Как это сделать, можно посмотреть
                                        <a class="color-yellow text-underline" href="#">тут</a>
                                    </div>
                                </div>
                            </div>
                            <div class="stages__item">
                                <div class="stages__item-count">
                                    <div class="stages__item-count-val">2</div>
                                </div>
                                <div class="stages__item-info">
                                    <div class="stages__item-title">Покупка курса</div>
                                    <div class="stages__item-text">
                                        После того как Вы определились с интересующим Вас курсом, заполняете форму заказа (необходимые
                                        контактные) и оплачиваете курс любым из удобных способов оплаты
                                    </div>
                                </div>
                            </div>
                            <div class="stages__item">
                                <div class="stages__item-count">
                                    <div class="stages__item-count-val">3</div>
                                </div>
                                <div class="stages__item-info">
                                    <div class="stages__item-title">Получение доступа к курсу</div>
                                    <div class="stages__item-text">
                                        После оплаты выбранного Вами курса, на указанный Вами адрес электронной почты мы отправим ссылку с
                                        открытым доступом к купленному Вами курсу
                                    </div>
                                </div>
                            </div>
                            <div class="stages__item">
                                <div class="stages__item-count stages__item-count--last">
                                    <div class="stages__item-count-val">4</div>
                                </div>
                                <div class="stages__item-info">
                                    <div class="stages__item-title">Прохождение курса</div>
                                    <div class="stages__item-text">
                                        Перейдя по ссылке, Вы сможете начать прохождение желанного курса! Для публикации курсов Мы
                                        используем
                                        сервис “Google class”
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="stages__contest-text stages__contest-text--mob">
                            После покупки курса вы сможете учавствовать в <a class="stages__contest-link" href="">конкурсе!</a>
                        </div>
                    </div>
                    <div class="stages__right">
                        <div class="stages__present">
                            <div class="stages__present-title">
                                Шанс выиграть $5000 и работу в New York
                            </div>


                            <button class="btn btn--default btn--red-dark js-open-modal" data-modal="buying-form-modal">
                                Купить курс
                            </button>
                        </div>
                    </div>
                    <div class="stages__contest-text">
                        После покупки курса вы сможете учавствовать в <a class="stages__contest-link" href="">конкурсе!</a>
                    </div>
                </div>
            </div>
        </div>

        <x-sections.support/>

    </div>


</x-layout>
