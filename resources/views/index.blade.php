<x-layout title="ICANNYC" header-class="header header--home">
    <x-sections.hero/>
    <x-sections.about/>
    <x-sections.advantages/>
    <x-sections.theme/>
    <x-sections.support/>
    <x-sections.theme-courses/>
    <x-sections.answers/>
    <x-sections.reviews/>
</x-layout>
