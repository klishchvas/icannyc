<header  class="{{$headerClass}}">
    <div class="container container--header">
        <div class="header__inner">
            <div class="header__left">
                <a class="logo header__logo" href="{{route('home')}}">
                    <img class="header__logo-img" src="{{asset('img/logo.svg')}}" alt="">
                </a>

                <div class="menu-burger" id="burger-menu">
                    <span class="menu-burger__el"></span>
                </div>

                <div class="menu-mobile" id="mobile-menu">
                    <nav class="menu">
                        <ul class="menu__list">
                            <li class="menu__item">
                                <a class="menu__link" href="{{route('courses.index')}}">Все курсы</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link js-open-modal" data-modal="conditions-modal">Конкурс</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link" href="javascript:void(0)">Контакты</a>
                            </li>
                            <x-partials.search-form-mobile/>
                        </ul>
                    </nav>
                </div>

            </div>
            <div class="header__right">
                <nav class="nav header__nav">
                    <ul class="nav__list">
                        <li class="nav__item">
                            <a class="nav__link" href="{{route('courses.index')}}">Все курсы</a>
                        </li>
                        <li class="nav__item"><a class="nav__link js-open-modal" href="#"
                                                 data-modal="conditions-modal">Конкурс</a>
                        </li>
                        <li class="nav__item"><a class="nav__link" href="#">Контакты</a></li>
                        <li class="nav__item"><a class="nav__link" href="#" id="open-search">Поиск</a></li>
                    </ul>
                    <x-partials.search-course/>
                </nav>

               <x-partials.audio/>

                <x-partials.language/>
            </div>
        </div>
    </div>
</header>
