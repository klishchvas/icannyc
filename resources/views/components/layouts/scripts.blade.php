<livewire:scripts />
@stack('scripts')
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
<script src="{{asset('js/vendor.js')}}"></script>
<script src="{{asset('js/accordion.js')}}"></script>
<script src="{{asset('js/countdown.js')}}"></script>
<script src="{{asset('js/modal.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
