
<div @isset($theme) theme="{{ $theme->id }}" @endisset card="{{ $course->id }}" class="course-card course-card--coming">
    <img class="course-card__preview" src="{{$course->picture}}" alt="">
    @if($course->picture_mobile)
        <img class="course-card__preview course-card__preview--mob" src="{{$course->picture_mobile}}" alt="">
    @endif
    <div class="course-card__inner course-card__inner--coming">
        <div class="course-card__data course-card__data--coming">
            <div class="course-card__header course-card__header--coming">
                <div class="course-card__name">{{$course->title}}</div>
            </div>
            <div class="course-card__release-info">СКОРО В ПРОДАЖЕ</div>
            <div class="course-card__price course-card__price--coming">
                <span class="course-card__price-val">${{$course->price}}</span>
            </div>
        </div>
        <div class="course-card__action course-card__action--coming">
            <button class="btn btn--default btn--yellow course-card__btn course-card__btn--coming">
                скоро в продаже
            </button>
        </div>
    </div>
    <div class="course-card__label course-card__label--new">
        Новое
    </div>
    <livewire:like :course="$course"/>
</div>
