<div @if($theme) theme="{{ $theme->id }}" @endif card="{{ $course->id }}" class="course-card">
    <img class="course-card__preview" src="{{$course->picture}}" alt="">
    <img class="course-card__preview course-card__preview--mob" src="{{$course->picture_mobile}}" alt="">
    <div class="course-card__inner course-card__inner--two-col">
        <div class="course-card__advantages">
            <div class="course-card__advantages-title">Что ты получишь?</div>
            <div class="course-card__advantages-list">
                <div class="course-card__advantages-item">Шанс выграть $5000</div>
                <div class="course-card__advantages-item">Финансовую независимость</div>
                <div class="course-card__advantages-item">Повышение заработка</div>
            </div>
        </div>
        <div class="course-card__data">
            <div class="course-card__header">
                <div class="course-card__name">{{$course->title}}</div>
                <div class="course-card__rating">
                    <span class="course-card__rating-val">{{$course->raiting}}</span>
                    <svg class="course-card__rating-star">
                        <use xlink:href="{{asset('img/sprite.svg#star')}}"></use>
                    </svg>
                </div>
            </div>
            @if ($course->old_price)
                <div class="countdown course-card__countdown" data-date-end="{{ \App\Models\Course::getTimerFormat() }}">
                    <div class="countdown__title course-card__countdown-title">
                        До конца <span class="color-yellow">акции</span>!
                    </div>
                    <div class="countdown__wrapper">
                        <div class="countdown__block" data-unit="hours">
                            <div class="countdown__item">
                                <span class="countdown__item-val">1</span>
                            </div>
                            <div class="countdown__item">
                                <span class="countdown__item-val">2</span>
                            </div>
                            <span class="countdown__unit-name">Часов</span>
                        </div>
                        <div class="countdown__spacer">:</div>
                        <div class="countdown__block" data-unit="minutes">
                            <div class="countdown__item">
                                <span class="countdown__item-val">5</span>
                            </div>
                            <div class="countdown__item">
                                <span class="countdown__item-val">2</span>
                            </div>
                            <span class="countdown__unit-name">Минут</span>
                        </div>
                        <div class="countdown__spacer">:</div>
                        <div class="countdown__block" data-unit="seconds">
                            <div class="countdown__item">
                                <span class="countdown__item-val">5</span>
                            </div>
                            <div class="countdown__item">
                                <span class="countdown__item-val">2</span>
                            </div>
                            <span class="countdown__unit-name">Секунд</span>
                        </div>
                    </div>
                </div>
            @endif
            <div class="course-card__price">
                <span class="course-card__price-text color-red">Всего:</span>
                <span class="course-card__price-val">${{$course->price}}</span>
                @if($course->old_price)
                    <span class="course-card__price-val-old">${{$course->old_price}}</span>
                @endif

            </div>
            <div class="course-card__action">
                <a href="{{route('courses.show', ['course' => $course])}}" class="btn btn--default btn--dark course-card__btn course-card__btn--more">
                    Подробнее
                </a>
                <button class="btn btn--default btn--red-light course-card__btn course-card__btn--buy js-open-modal" data-modal="buying-form-modal">
                    <span>Купить курс</span>
                    <span class="course-card__btn-buy-price">{{$course->price}}$</span>
                </button>
            </div>
        </div>
    </div>
    @foreach($course->tags as $tag)
        <div class="course-card__label course-card__label--new">
            {{$tag->title}}
        </div>
    @endforeach
    <livewire:like :course="$course"/>
</div>
