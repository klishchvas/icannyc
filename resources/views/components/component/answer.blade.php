<div class="accordion__item">
    <button class="btn accordion__btn" aria-expanded="false">
        <span class="accordion__title">{{$answer->title}}</span>
        <svg class="accordion__icon">
            <use xlink:href="img/sprite.svg#arrow-bottom"></use>
        </svg>
    </button>
    <div class="accordion__collapse">
        <div class="accordion__body">{{$answer->description}}</div>
    </div>
</div>
