<div class="swiper-slide reviews-slider__slide">
    <div class="review-card">
        <div class="review-card__header">
            <img class="review-card__ava" src="{{asset('img/ava.png')}}" alt="">
            <div class="review-card__info">
                <div class="review-card__name">{{$review->name}}</div>
                <div class="review-card__row">
                    <span class="review-card__city">{{$review->city}}</span>
                    <span class="review-card__age">{{$review->age}}</span>
                </div>
                <div class="star-rating review-card__rating">
                    @for($i = 0; $i < $review->mark;$i++)
                        <span class="star-rating__item">★</span>
{{--                        <span class="star-rating__item">★</span>
                        <span class="star-rating__item">★</span>
                        <span class="star-rating__item">★</span>
                        <span class="star-rating__item">★</span>--}}
                    @endfor
                </div>
                <div class="review-card__course-name">
                    {{$review->course}}
                </div>
            </div>
        </div>
        <div class="review-card__body">{{$review->review}}</div>
    </div>
</div>
