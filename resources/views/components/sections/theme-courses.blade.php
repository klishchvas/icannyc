<div class="relevant-courses">
    <div class="container relevant-courses__container">
        <div class="relevant-courses__header">
            <div class="block-title block-title--md relevant-courses__title">
                Мы подобрали курсы <span class="color-yellow">специально для тебя</span>!
            </div>
            <a class="more relevant-courses__more" href="{{route('courses.index')}}">
                <span class="more__text">Смотреть все</span>
                <svg class="more__arrow">
                    <use xlink:href="img/sprite.svg#arrow-v2"></use>
                </svg>
            </a>
        </div>
        @foreach(\App\Models\Theme::all() as $number=>$theme)
            <x-pages.course-card  :course="$theme->course" :theme="$theme" />
        @endforeach
    </div>
    <div class="relevant-courses__stat">
        <div class="container relevant-courses__stat-container">
            <div class="relevant-courses__stat-inner">
                <div class="relevant-courses__stat-item">
                    <div class="relevant-courses__stat-item-title">
            <span class="purecounter"
                  data-purecounter-start="0"
                  data-purecounter-end="1955"
                  data-purecounter-duration="3">0</span>
                        <span>+</span>
                    </div>
                    <div class="relevant-courses__stat-item-subtitle">Человек купили курсы</div>
                </div>
                <div class="relevant-courses__stat-item">
                    <div class="relevant-courses__stat-item-title">
            <span class="purecounter"
                  data-purecounter-start="0"
                  data-purecounter-end="40"
                  data-purecounter-duration="3">0</span>
                        <span>+</span>
                    </div>
                    <div class="relevant-courses__stat-item-subtitle">Опытных спикеров</div>
                </div>
                <div class="relevant-courses__stat-item">
                    <div class="relevant-courses__stat-item-title">
            <span class="purecounter"
                  data-purecounter-start="0"
                  data-purecounter-end="35"
                  data-purecounter-duration="3">0</span>
                        <span>+</span>
                    </div>
                    <div class="relevant-courses__stat-item-subtitle">Разных курсов</div>
                </div>
            </div>
        </div>
    </div>
    <div class="relevant-courses__footer">
        <livewire:forms.mailing.form-mailing />
        <div class="relevant-courses__team-wrap">
            <div class="relevant-courses__team">
                <img class="relevant-courses__team-photo" src="{{asset('img/hero/hero-team1.png')}}" alt="">
                <img class="relevant-courses__team-photo" src="{{asset('img/hero/hero-team2.png')}}" alt="">
                <img class="relevant-courses__team-photo" src="{{asset('img/hero/hero-team3.png')}}" alt="">
                <img class="relevant-courses__team-photo" src="{{asset('img/hero/hero-team4.png')}}" alt="">
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            /* Themes */

            let idTheme = 0;
            let oneCourse;
            let relevantCourses = document.querySelectorAll('.course-card');
            const themes = document.querySelectorAll('.theme_checkbox');


            relevantCourses.forEach((item) => {
                item.style.display = 'none';
            });

            themes.forEach(function (theme) {
                theme.addEventListener('click', function (e) {
                    e.stopImmediatePropagation();
                    this.classList.toggle('active');
                    if (this.classList.contains('active')) {
                        idTheme = this.value;
                        getCourses(idTheme);
                    } else {
                        idTheme = null;
                        getCourses(idTheme);
                    }
                });
            });

            function getCourses(idTheme) {
                relevantCourses = document.querySelectorAll('.course-card');
                relevantCourses.forEach((item) => {
                    if (idTheme) {
                        if (item.getAttribute('theme') === idTheme) {

                            oneCourse = document.querySelector(`.course-card[theme="${idTheme}"]`);
                            oneCourse.style.display = 'block';

                        }
                    } else {
                        item.style.display = 'none';
                    }


                });
            }
        </script>
    @endpush
</div>
