<div class="support">
    <div class="container support__container">
        <div class="support__inner">
            <div class="support__info">
                <div class="block-title block-title--md block-title--black support__title">
                    Поддержка на всех этапах прохождения курса
                </div>
                <div class="support__subtitle">
                    При покупке курса у нас, ты получишь качественную поддержку в нашем telegram канале
                </div>
                <div class="support__info-btns">
                    <button class="btn btn--red-dark btn--icon support__info-btn">
                        <span>Перейти в наш telegram канал</span>
                        <svg>
                            <use xlink:href="img/sprite.svg#telegram"></use>
                        </svg>
                    </button>
                    <button class="btn btn--yellow btn--center support__info-btn js-open-support">
                        Подписаться на e-mail рассылку
                    </button>
                </div>
            </div>
            {{-- livewire-component --}}
            <livewire:forms.mailing.form-mailing-top/>
            {{-- ================================= --}}
        </div>
    </div>
</div>

