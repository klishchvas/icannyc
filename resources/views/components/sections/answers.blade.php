<div class="answers">
    <div class="container answers__container">
        <div class="block-title block-title--space answers__title">
            <span class="color-yellow">Ответы</span> на популярные вопросы
        </div>

        <div class="accordion answers__accordion">
            @foreach ($answers as $answer)
                <x-component.answer :answer="$answer" />
            @endforeach
        </div>
    </div>
</div>
