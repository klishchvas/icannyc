@if($reviews)
    <div class="reviews">
        <div class="container reviews__container">
            <div class="block-title block-title--space reviews__title">
                <span class="color-yellow">Отзывы</span> о наших курсах
            </div>
            <div class="reviews__note">
                <div class="reviews__subtitle">Читай отывы о наших курсах и спикерах!</div>
                <div class="reviews__more">
                    <div class="reviews__more-text">Смотрите так же отзывы в:</div>
                    <a class="reviews__more-link" href="#" target="_blank">
                        <svg class="reviews__more-icon reviews__more-icon--inst">
                            <use xlink:href="img/sprite.svg#instagram"></use>
                        </svg>
                    </a>
                    <a class="reviews__more-link" href="#" target="_blank">
                        <svg class="reviews__more-icon reviews__more-icon--vk">
                            <use xlink:href="img/sprite.svg#vk"></use>
                        </svg>
                    </a>
                </div>
            </div>

            <div class="reviews-slider">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        @foreach($reviews as $review)
                            <x-component.review :review="$review"/>
                        @endforeach
                    </div>
                </div>
                <div class="reviews-slider__btn reviews-slider__btn--prev">
                    <svg>
                        <use xlink:href="img/sprite.svg#arrow-left"></use>
                    </svg>
                </div>
                <div class="reviews-slider__btn reviews-slider__btn--next">
                    <svg>
                        <use xlink:href="img/sprite.svg#arrow-left"></use>
                    </svg>
                </div>
            </div>
        </div>
        <div class="reviews__slide-hint">
            <span>Листайте вправо</span>
            <img class="" src="{{asset('img/swipe.png')}}" alt="">
        </div>
    </div>
@endif
