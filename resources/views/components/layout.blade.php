<!DOCTYPE html>
<html lang="ru">

<x-layouts.head  :title=$title />

<body>
<div class="site-container">

    <x-layouts.header :header-class="$headerClass" />

    {{$slot}}

    <x-layouts.footer/>
    <x-layouts.modals />

</div>
<x-layouts.scripts />

</body>
</html>

