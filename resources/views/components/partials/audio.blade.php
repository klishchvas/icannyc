<div class="player">
    <audio autoplay="true" id="audiotag" src="{{ asset('audio.mp3') }}"></audio>
    @push('scripts')
        <script type="text/javascript">
            function play(){
                document.getElementById('audiotag').play();

            }
            document.getElementById('play').click();
        </script>
    @endpush
    <button id="play" onclick="play()" class="btn player__btn">
        <svg class="player__btn-icon">
            <use xlink:href="{{ asset('img/sprite.svg#pause') }}"></use>
        </svg>
    </button>
    <span class="player__name">
        Alicia Keys - New York...
      </span>
</div>
