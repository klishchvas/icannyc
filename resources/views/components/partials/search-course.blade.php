<form action="{{ route('courses.search') }}" class="search-form @if(request('courseSearch')) search-form--open @endif" id="search-form" autocomplete="off">
    @csrf
    <div class="search-form__inner">
        <input
            type="text"
            required
            class="input input--default search-form__input"
            name="courseSearch"
            id="course-search"
            value="{{ request('courseSearch') }}"
            placeholder="Курс по дизайну"
        >
        <button class="btn btn--default btn--yellow search-form__btn-search">
            Поиск
        </button>
        <button class="btn search-form__btn-close" id="close-search">
            <svg>
                <use xlink:href="{{ asset('/img/sprite.svg#close') }}"></use>
            </svg>
        </button>
    </div>
</form>
