<form action="{{ route('courses.search') }}" class="search-form search-form--mobile" autocomplete="off">
    @csrf
    <div class="search-form__inner">
        <input
            type="text"
            required
            class="input input--default search-form__input search-form__input--mobile"
            name="courseSearch"
            placeholder="Курс по дизайну"
            value="{{ request('courseSearch') }}"
        >
        <button class="btn btn--default btn--yellow search-form__btn-search search-form__btn-search--mobile">
            Поиск
        </button>
    </div>
</form>
