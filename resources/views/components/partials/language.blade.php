<style>
    .yt-button.yt-button_type_left{
        display: none!important;
    }
    .yt-button.yt-button_type_right{
        border: 0px solid black!important;
        background: none!important;
    }
    .yt-button__text{
        font-family: "Cuprum", sans-serif!important;
        font-weight: 400!important;
        font-size: 24px!important;
        color: #fff!important;
    }
    .yt-button__icon.yt-button__icon_type_right{
        display: none!important;
    }
    .yt-wrapper.yt-wrapper_align_right{
        display: none!important;
    }
</style>
<div class="language header__language">
    <div id="ytWidget"></div><script src="https://translate.yandex.net/website-widget/v1/widget.js?widgetId=ytWidget&pageLang=ru&widgetTheme=light&autoMode=true" type="text/javascript"></script>
{{--                    <div class="language__box">--}}
{{--                        <button class="btn language__btn">--}}
{{--                            <span class="language__active-name">RU</span>--}}
{{--                        </button>--}}
{{--                        <div class="language__list-wrapper">--}}
{{--                            <div class="language__list">--}}
{{--                                <button class="btn language__list-btn" disabled>RU</button>--}}
{{--                                <button class="btn language__list-btn">EN</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
</div>
