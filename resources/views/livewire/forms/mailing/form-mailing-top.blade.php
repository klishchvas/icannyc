<div class="support__form-wrap">
    <div class="support__form-bg">
        <form wire:submit.prevent="submit()" class="support__form" action="">
            <div class="support__form-title">Подписаться на рассылку</div>
            <input wire:model.defer="name" class="input input--default support__form-input" type="text"
                   placeholder="Введите имя">
            @error('name') <div class="error"> {{$message}} </div> @enderror
            <input wire:model.defer="email" class="input input--default support__form-input" type="email"
                   placeholder="Введите E-mail">
            @error('email') <div class="error"> {{$message}} </div> @enderror
            <button type="submit" class="btn btn--default btn--red-dark btn--center support__form-btn">
                Быть в курсе событий
            </button>
            <label class="custom-checkbox" for="support-mail1">
                <input wire:model.defer="agreement" type="checkbox" name="mail-chimp" id="support-mail1">
                <div class="custom-checkbox__inner custom-checkbox__inner--inline">
                    <span class="custom-checkbox__icon custom-checkbox__icon--red"></span>
                    <span class="custom-checkbox__text custom-checkbox__text--inline">
                Я принимаю <a class="text-underline" href="#">политику</a> конфиденциальности сайта
              </span>
                </div>
            </label>
            @error('agreement')<div class="error"> {{$message}} </div> @enderror

            @if (session()->has('success_message'))
                <div class="success">{{session('success_message')}}</div>
            @else
                @error('fail_mailing')
                <div class="error"> {{$message}} </div> @enderror
            @endif
        </form>

    </div>

</div>
