<div class="relevant-courses__mail">
    <div class="relevant-courses__mail-title">
        ПОДПИШИСЬ НА РАССЫЛКУ
    </div>
    <form wire:submit.prevent="submit()" class="mail-chimp" action="">
        <div class="mail-chimp__header">
            <div class="mail-chimp__hint">Будь в курсе всех новостей и новых продуктов</div>
            <label class="custom-checkbox mail-chimp__privacy" for="mail-chimp">
                <input wire:model.defer="agreement" type="checkbox" name="mail-chimp" id="mail-chimp">
                <div class="custom-checkbox__inner custom-checkbox__inner--inline">
                    <span class="custom-checkbox__icon custom-checkbox__icon--yellow"></span>
                    <span class="custom-checkbox__text custom-checkbox__text--inline">
                Я принимаю <a class="text-underline" href="#">политику</a> конфиденциальности сайта
              </span>
                </div>
            </label>
            @error('agreement') <div class="error"> {{$message}} </div> @enderror
        </div>
        <div class="mail-chimp__form-group relevant-courses__form-group">
            <input wire:model.defer="name"  class="input input--default input--bg-dark mail-chimp__input relevant-courses__mail-input"
                   type="text"
                   placeholder="Введите имя">

            <input wire:model.defer="email" class="input input--default input--bg-dark mail-chimp__input relevant-courses__mail-input"
                   type="email"
                   placeholder="Введите E-mail">

            <button type="submit" class="btn btn--default btn--yellow btn--center relevant-courses__mail-btn">
                Быть в курсе событий
            </button>
        </div>
        <br>
        @error('email') <div class="error"> {{$message}} </div> @enderror
        @error('name') <div class="error"> {{$message}} </div> @enderror


    </form>
    @if (session()->has('success_message'))
        <div class="success">{{session('success_message')}}</div>
        @else
        @error('fail_mailing') <div class="error"> {{$message}} </div> @enderror

    @endif
</div>
