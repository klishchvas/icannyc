<div class="course-card__likes" wire:click.prevent="addLike()">
    <svg class="course-card__likes-icon">
        <use xlink:href="{{asset('img/sprite.svg#heart')}}"></use>
    </svg>
    <span class="course-card__likes-count">{{$course->likes}}</span>
</div>
